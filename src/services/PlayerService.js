var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { MatchService } from '../services/MatchService';
var PlayerService = (function () {
    function PlayerService(matchS) {
        this.matchS = matchS;
        this.initialMatchValues = { sets: 0, legs: 0, score: 0, average: '0', matchScores: [], frequentScores: [] };
        this.legs = [];
        this.lastLegFinalScores = [];
        this.newLeg = true;
        this.players = [];
        this.setStarter = 0;
        this.legStarter = 0;
        this.activePlayer = 0;
        this.turn = 1;
        this.relativeTurn = 1;
    }
    ;
    PlayerService.prototype.setInitialValues = function () {
        for (var i = 0; i < this.players.length; i++) {
            var match = this.players[i].match;
            match.score = this.matchS.legPoints;
        }
    };
    PlayerService.prototype.initializePlayers = function () {
        this.initialMatchValues = { sets: 0, legs: 0, score: 0, average: '0', matchScores: [], frequentScores: [] };
        this.legs = [];
        this.lastLegFinalScores = [];
        this.newLeg = true;
        this.players = [];
        this.setStarter = 0;
        this.legStarter = 0;
        this.activePlayer = 0;
        this.turn = 1;
        this.relativeTurn = 1;
    };
    PlayerService.prototype.addPlayer = function (name) {
        var player = { id: this.players.length, name: name, match: JSON.parse(JSON.stringify(this.initialMatchValues)) };
        this.players.push(player);
    };
    PlayerService.prototype.removePlayer = function (id) {
        for (var i = 0; i < this.players.length; i++) {
            if (this.players[i].id === id) {
                this.players.splice(i, 1);
                break;
            }
        }
    };
    PlayerService.prototype.setLastLegFinalScores = function () {
        for (var i = 0; i < this.players.length; i++) {
            var match = this.players[i].match;
            this.lastLegFinalScores.push(match.score);
        }
    };
    PlayerService.prototype.updateTurn = function () {
        this.relativeTurn++;
        if (this.relativeTurn === this.players.length + 1) {
            this.turn++;
            this.relativeTurn = 1;
        }
    };
    PlayerService.prototype.matchStarted = function () {
        var playerStarted = [];
        for (var i = 0; i < this.players.length; i++) {
            var match = this.players[i].match;
            if (match.sets === 0 && match.legs === 0 && match.score === this.matchS.legPoints)
                playerStarted.push(false);
            else
                playerStarted.push(true);
        }
        for (var j = 0; j < playerStarted.length; j++) {
            if (playerStarted[j] === true)
                return true;
        }
        return false;
    };
    return PlayerService;
}());
PlayerService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [MatchService])
], PlayerService);
export { PlayerService };
//# sourceMappingURL=PlayerService.js.map