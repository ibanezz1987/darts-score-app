var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from '@angular/core';
var MatchService = (function () {
    function MatchService() {
        this.setsToWin = 5;
        this.legsToWin = 3;
        this.legPoints = 501;
    }
    MatchService.prototype.updateSetsToWin = function (value) {
        this.setsToWin = value;
    };
    MatchService.prototype.updateLegsToWin = function (value) {
        this.legsToWin = value;
    };
    MatchService.prototype.updateLegPoints = function (value) {
        this.legPoints = value;
    };
    return MatchService;
}());
MatchService = __decorate([
    Injectable()
], MatchService);
export { MatchService };
//# sourceMappingURL=MatchService.js.map