var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { CalculationService } from '../services/CalculationService';
var StatisticsService = (function () {
    function StatisticsService(calcS) {
        this.calcS = calcS;
    }
    ;
    StatisticsService.prototype.updateFrequentScores = function (matchScores) {
        // create scoreFrequency array
        var scoreFrequency = [];
        scoreFrequency = this.getScoreFrequentees(scoreFrequency, matchScores);
        scoreFrequency.sort(this.calcS.compareFrequency);
        // push most frequent in array
        var maxFrequentScores = 4;
        var scores = [];
        for (var j = 0; j < scoreFrequency.length; j++) {
            scores.push({ value: scoreFrequency[j].value, amount: scoreFrequency[j].amount });
            if (scores.length === maxFrequentScores) {
                break;
            }
        }
        return scores;
    };
    StatisticsService.prototype.getScoreFrequentees = function (scoreFrequency, matchScores) {
        // create scoreFrequency
        for (var i = 0; i < matchScores.length; i++) {
            var matchScore = matchScores[i].score;
            var isExisting = false;
            for (var ii = 0; ii < scoreFrequency.length; ii++) {
                // als score vaker voorkomt
                if (matchScore == scoreFrequency[ii].value) {
                    isExisting = true;
                    scoreFrequency[ii].amount += 1;
                    break;
                }
            }
            // 0 niet meenemen
            if (matchScore === 0) {
                continue;
            }
            // als score 1 keer voor komt
            if (!isExisting) {
                scoreFrequency.push({ value: matchScore, amount: 1 });
            }
        }
        return scoreFrequency;
    };
    return StatisticsService;
}());
StatisticsService = __decorate([
    Injectable(),
    __metadata("design:paramtypes", [CalculationService])
], StatisticsService);
export { StatisticsService };
//# sourceMappingURL=StatisticsService.js.map