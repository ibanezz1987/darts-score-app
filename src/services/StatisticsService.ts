import { Injectable } from '@angular/core';
import { CalculationService } from '../services/CalculationService';

@Injectable()
export class StatisticsService {

    constructor(
        private calcS: CalculationService) {
    };

    calculateStatistics(player) {
        // pMT = player.matchTracker, array met sets, legs, scores-info
        let pMT = player.match.matchTracker;

        player.statistics = {
            matchAvg:           this.getMatchAvg(pMT),
            first9LegAvg:       this.getFirst9LegAvg(pMT),
            bestLegAvg:         this.getBestLegAvg(pMT),
            currentLegAvg:      this.getCurrentLegAvg(pMT),
            lastLegAvg:         this.getLastLegAvg(pMT),
            totalLegsWon:       this.getTotalLegsWon(pMT),
            totalBreaks:        this.getTotalBreaks(pMT),
            highestScore:       this.getHighestScore(pMT),
            highestCheckout:    this.getHighestCheckout(pMT),
            avgCheckout:        this.getAvgCheckout(pMT),
            cat5Checkouts:      this.getScoresInRange(pMT, true, 170, 170),
            cat4Checkouts:      this.getScoresInRange(pMT, true, 121, 167),
            cat3Checkouts:      this.getScoresInRange(pMT, true, 101, 120),
            cat2Checkouts:      this.getScoresInRange(pMT, true, 41, 100),
            cat1Checkouts:      this.getScoresInRange(pMT, true, 2, 40),
            no180Scores:        this.getScoresInRange(pMT, false, 180, 180),
            no140Scores:        this.getScoresInRange(pMT, false, 140, 177),
            no100Scores:        this.getScoresInRange(pMT, false, 100, 139),
            no60Scores:         this.getScoresInRange(pMT, false, 60, 99),
            no0Scores:          this.getScoresInRange(pMT, false, 0, 59),
            no9DartsToWin:      this.getDartsToWin(pMT, 9, 9),
            no10DartsToWin:     this.getDartsToWin(pMT, 10, 12),
            no13DartsToWin:     this.getDartsToWin(pMT, 13, 15),
            no16DartsToWin:     this.getDartsToWin(pMT, 16, 18),
            no19DartsToWin:     this.getDartsToWin(pMT, 19, 21),
            no22DartsToWin:     this.getDartsToWin(pMT, 22, 24),
            no25DartsToWin:     this.getDartsToWin(pMT, 25, 27),
            no28DartsToWin:     this.getDartsToWin(pMT, 28, 30),
            no31DartsToWin:     this.getDartsToWin(pMT, 31, Infinity),
        }
    }

    // Averages

    getMatchAvg(pMT):number {
        let arrows:number = 0;
        let sum:number = 0;
        let matchAvg:number = 0;

        // Loop through each set
        for(let i = 0; i < pMT.length; i++) {
            let set:any = pMT[i];

            // Loop through each leg
            for(let j = 0; j < set.length; j++) {
                let leg:any = set[j];

                // Loop through scores
                for(let k = 0; k < leg.length; k++) {
                    let scoreInfo:any = leg[k];

                    arrows += scoreInfo.arrows;
                    sum += scoreInfo.score;
                }
            }
        }
        
        matchAvg = sum / arrows * 3;

        if(isNaN(matchAvg)) {
            return 0;
        }

        return matchAvg;
    }

    getFirst9LegAvg(pMT):number {
        let arrows:number = 0;
        let sum:number = 0;
        let first9Avg:number = 0;

        // Loop through each set
        for(let i = 0; i < pMT.length; i++) {
            let set:any = pMT[i];

            // Loop through each leg
            for(let j = 0; j < set.length; j++) {
                let leg:any = set[j];

                // Loop through scores
                for(let k = 0; k < leg.length && k < 3; k++) { // Prevent looping through more than 3 turns / 9 darts
                    let scoreInfo:any = leg[k];

                    arrows += scoreInfo.arrows;
                    sum += scoreInfo.score;
                }
            }
        }
        
        first9Avg = sum / arrows * 3;

        if(isNaN(first9Avg)) {
            return 0;
        }

        return first9Avg;
    }
    
    getBestLegAvg(pMT):number {
        let legAverages:number[] = [];
        let highestAvg:number = 0;

        if(pMT.length === 1 && pMT[0].length === 1) {
            return 0;
        }

        // Loop through each set
        for(let i = 0; i < pMT.length; i++) {
            let set:any = pMT[i];

            // Loop through each leg
            for(let j = 0; j < set.length; j++) {
                let leg:any = set[j];
                let arrows:number = 0;
                let sum:number = 0;

                // Loop through scores
                for(let k = 0; k < leg.length; k++) {
                    let scoreInfo:any = leg[k];

                    arrows += scoreInfo.arrows;
                    sum += scoreInfo.score;
                }

                let legAvg:number = sum / arrows * 3;
                
                if(!isNaN(legAvg)) {
                    legAverages.push(legAvg);
                }
            }
        }

        highestAvg = Math.max.apply(Math, legAverages);

        if(isNaN(highestAvg)) {
            return 0;
        }
        
        return highestAvg;
    }
    
    getCurrentLegAvg(pMT):number {
        let legAvg:number = 0;
        let set:any = pMT[pMT.length - 1];
        let leg:any = set[set.length - 1];
        let arrows:number = 0;
        let sum:number = 0;

        // Loop through scores
        for(let k = 0; k < leg.length; k++) {
            let scoreInfo:any = leg[k];

            arrows += scoreInfo.arrows;
            sum += scoreInfo.score;
        }

        legAvg = sum / arrows * 3;

        if(isNaN(legAvg)) {
            return 0;
        }
        
        return legAvg;
    }
    
    getLastLegAvg(pMT):number {
        let legAvg:number = 0;
        let set:any = pMT[pMT.length - 1];
        let leg:any = {};
        let arrows:number = 0;
        let sum:number = 0;

        if(pMT.length === 1 && set.length === 1) {
            // current leg = first set, first leg
            return 0;
        }
        else if(set.length === 1) {
            // current leg = First leg of set
            set = pMT[pMT.length - 2];
            leg = set[set.length - 1];
        }
        else {
            leg = set[set.length - 2];
        }

        // Loop through scores
        for(let k = 0; k < leg.length; k++) {
            let scoreInfo:any = leg[k];

            arrows += scoreInfo.arrows;
            sum += scoreInfo.score;
        }

        legAvg = sum / arrows * 3;

        if(isNaN(legAvg)) {
            return 0;
        }
        
        return legAvg;
    }


    // Records

    getTotalLegsWon(pMT):number {
        let legsWon:number = 0;

        // Loop through each set
        for(let i = 0; i < pMT.length; i++) {
            let set:any = pMT[i];

            // Loop through each leg
            for(let j = 0; j < set.length; j++) {
                let leg:any = set[j];

                // Loop through scores
                for(let k = 0; k < leg.length; k++) {
                    let score:any = leg[k];

                    if(score.hasFinished) {
                        legsWon++;
                    }
                }
            }
        }
        
        return legsWon;
    }

    getTotalBreaks(pMT):number {
        let legsWon:number = 0;

        // Loop through each set
        for(let i = 0; i < pMT.length; i++) {
            let set:any = pMT[i];

            // Loop through each leg
            for(let j = 0; j < set.length; j++) {
                let leg:any = set[j];

                // Loop through scores
                for(let k = 0; k < leg.length; k++) {
                    let score:any = leg[k];

                    if(score.hasFinished && !score.isLegStarter) {
                        legsWon++;
                    }
                }
            }
        }
        
        return legsWon;
    }

    getHighestScore(pMT):number {
        let highestScore:number = 0;

        // Loop through each set
        for(let i = 0; i < pMT.length; i++) {
            let set:any = pMT[i];

            // Loop through each leg
            for(let j = 0; j < set.length; j++) {
                let leg:any = set[j];

                // Loop through scores
                for(let k = 0; k < leg.length; k++) {
                    let score:any = leg[k];

                    if(score.score > highestScore) {
                        highestScore = score.score;
                    }
                }
            }
        }
        
        return highestScore;
    }

    getHighestCheckout(pMT):number {
        let highestCheckout:number = 0;

        // Loop through each set
        for(let i = 0; i < pMT.length; i++) {
            let set:any = pMT[i];

            // Loop through each leg
            for(let j = 0; j < set.length; j++) {
                let leg:any = set[j];

                // Loop through scores
                for(let k = 0; k < leg.length; k++) {
                    let score:any = leg[k];

                    if(score.hasFinished && score.score > highestCheckout) {
                        highestCheckout = score.score;
                    }
                }
            }
        }
        
        return highestCheckout;
    }

    getAvgCheckout(pMT):number {
        let sum:number = 0;
        let total:number = 0;
        let checkoutAvg:number = 0;

        // Loop through each set
        for(let i = 0; i < pMT.length; i++) {
            let set:any = pMT[i];

            // Loop through each leg
            for(let j = 0; j < set.length; j++) {
                let leg:any = set[j];

                // Loop through scores
                for(let k = 0; k < leg.length; k++) {
                    let score:any = leg[k];

                    if(score.hasFinished) {
                        sum += score.score;
                        total++;
                    }
                }
            }
        }
        
        checkoutAvg = sum / total;

        if(isNaN(checkoutAvg)) {
            return 0;
        }
        
        return checkoutAvg;
    }


    // Scores, Checkouts

    getScoresInRange(pMT, onlyCheckouts, min, max):number {
        let scoresInRange:number = 0;

        // Loop through each set
        for(let i = 0; i < pMT.length; i++) {
            let set:any = pMT[i];

            // Loop through each leg
            for(let j = 0; j < set.length; j++) {
                let leg:any = set[j];

                // Loop through scores
                for(let k = 0; k < leg.length; k++) {
                    let score:any = leg[k];

                    if(onlyCheckouts && score.hasFinished && score.score >= min && score.score <= max) {
                        scoresInRange++
                    }
                    else if(!onlyCheckouts && score.score >= min && score.score <= max) {
                        scoresInRange++
                    }
                }
            }
        }
        
        return scoresInRange;
    }

    // Darts to win
    getDartsToWin(pMT, min, max):number {
        let dartsToWinInRange:number = 0;

        // Loop through each set
        for(let i = 0; i < pMT.length; i++) {
            let set:any = pMT[i];

            // Loop through each leg
            for(let j = 0; j < set.length; j++) {
                let leg:any = set[j];
                let totalDarts:number = 0;

                // Skip empty legs
                if(!leg.length) {
                    break;
                }

                let hasFinished:boolean = leg[leg.length - 1].hasFinished;

                // Skip leg if its lost
                if(!hasFinished) {
                    break;
                }

                // Loop through scores
                for(let k = 0; k < leg.length; k++) {
                    let score:any = leg[k];

                    totalDarts += score.arrows;
                }

                if(totalDarts >= min && totalDarts <= max) {
                    dartsToWinInRange++;
                }
            }
        }

        return dartsToWinInRange;
    }


    // Frequent scores
    updateFrequentScores(player):{value: number, amount: number}[] {
        // pMT = player.matchTracker, array met sets, legs, scores-info
        let pMT = player.match.matchTracker;
        let scoreFrequency:number[] = [];
        let maxFrequentScores:number = 4;
        let frequentScores:any[] = [];

        scoreFrequency = this.getScoreFrequentees(pMT);

        for(let i=scoreFrequency.length - 1; i >= 0; i--) {
            if(scoreFrequency[i] > 0 && i !== 0) {
                frequentScores.push({value: i, amount: scoreFrequency[i]});
            }
        }

        frequentScores.sort(this.calcS.compareFrequency);               // Sort
        frequentScores = frequentScores.slice(0, maxFrequentScores);    // Remove lower results

        return frequentScores;
    }

    getScoreFrequentees(pMT):number[] {
        // Create an array of length 181 filled with zeros
        let frequencyArray:number[] = Array(181).fill(0);

        // Loop through each set
        for(let i = 0; i < pMT.length; i++) {
            let set:any = pMT[i];

            // Loop through each leg
            for(let j = 0; j < set.length; j++) {
                let leg:any = set[j];
                
                // Loop through scores
                for(let k = 0; k < leg.length; k++) {
                    let score:any = leg[k];

                    frequencyArray[score.score]++
                }
            }
        }

        return frequencyArray;
    }

}
