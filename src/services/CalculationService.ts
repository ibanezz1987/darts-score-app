import { Injectable } from '@angular/core';

@Injectable()
export class CalculationService {

    // validScores example [..., [178]: false, [179]:false [180]: true]
    validThrowScores:boolean[] = this.returnValidThrowScores();
    validFinishScores:boolean[] = this.returnValidFinishScores();

    // calc service
    compareFrequency(a,b):number {
        if (a.amount < b.amount)
            return 1;
        if (a.amount > b.amount)
            return -1;
        return 0;
    }

    validThrowScoreCheck(score:number):boolean {
        // if score is greather than array values
        if(score > this.validThrowScores.length -1) {
            return false;
        }

        // if match
        if(this.validThrowScores[score]) {
            return true;
        }

        return false;
    }

    returnValidThrowScores():boolean[] {
        let validScores:boolean[] = [true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,false,true,true,false,true,true,false,true,true,false,false,true,false,false,true,false,false,true];

        return validScores;
    }

    validFinishScoreCheck(score:number):boolean {
        // if score is greather than array values
        if(score > this.validFinishScores.length -1) {
            return false;
        }

        // if match
        if(this.validFinishScores[score]) {
            return true;
        }

        return false;
    }

    returnValidFinishScores():boolean[] {
        let validScores:boolean[] =
                       [false,false,true,true,true,true,true,true,true,true,true,
                               true,true,true,true,true,true,true,true,true,true,
                               true,true,true,true,true,true,true,true,true,true,
                               true,true,true,true,true,true,true,true,true,true,
                               true,true,true,true,true,true,true,true,true,true,

                               true,true,true,true,true,true,true,true,true,true,
                               true,true,true,true,true,true,true,true,true,true,
                               true,true,true,true,true,true,true,true,true,true,
                               true,true,true,true,true,true,true,true,true,true,
                               true,true,true,true,true,true,true,true,true,true,

                               true,true,true,true,true,true,true,true,true,true,
                               true,true,true,true,true,true,true,true,true,true,
                               true,true,true,true,true,true,true,true,true,true,
                               true,true,true,true,true,true,true,true,true,true,
                               true,true,true,true,true,true,true,true,true,true,

                               true,true,true,true,true,true,true,true,false,true,
                               true,false,false,true,false,false,true,false,false,true];
        return validScores;
    }

    checkInpossibleFinishes(score, darts):boolean {
        let validScores:boolean[] = [];

        if(darts === 1) {
            validScores = [false,false,true,false,true,false,true,false,true,false,true,
                                  false,true,false,true,false,true,false,true,false,true,
                                  false,true,false,true,false,true,false,true,false,true,
                                  false,true,false,true,false,true,false,true,false,true,
                                  false,false,false,false,false,false,false,false,false,true,

                                  false,false,false,false,false,false,false,false,false,false,
                                  false,false,false,false,false,false,false,false,false,false,
                                  false,false,false,false,false,false,false,false,false,false,
                                  false,false,false,false,false,false,false,false,false,false,
                                  false,false,false,false,false,false,false,false,false,false,

                                  false,false,false,false,false,false,false,false,false,false,
                                  false,false,false,false,false,false,false,false,false,false,
                                  false,false,false,false,false,false,false,false,false,false,
                                  false,false,false,false,false,false,false,false,false,false,
                                  false,false,false,false,false,false,false,false,false,false,

                                  false,false,false,false,false,false,false,false,false,false,
                                  false,false,false,false,false,false,false,false,false,false];
        }
        else if (darts === 2) {
            validScores = [false,false,true,true,true,true,true,true,true,true,true,
                                  true,true,true,true,true,true,true,true,true,true,
                                  true,true,true,true,true,true,true,true,true,true,
                                  true,true,true,true,true,true,true,true,true,true,
                                  true,true,true,true,true,true,true,true,true,true,

                                  true,true,true,true,true,true,true,true,true,true,
                                  true,true,true,true,true,true,true,true,true,true,
                                  true,true,true,true,true,true,true,true,true,true,
                                  true,true,true,true,true,true,true,true,true,true,
                                  true,true,true,true,true,true,true,true,false,true,

                                  true,false,false,true,false,false,true,false,false,true,
                                  false,false,false,false,false,false,false,false,false,false,
                                  false,false,false,false,false,false,false,false,false,false,
                                  false,false,false,false,false,false,false,false,false,false,
                                  false,false,false,false,false,false,false,false,false,false,

                                  false,false,false,false,false,false,false,false,false,false,
                                  false,false,false,false,false,false,false,false,false,false];
        }

        return !validScores[score];
    }

    returnCheckout(score: number):string {
        let invalid:string = 'Invalid score';
        let noFinish:string = 'No finish';
        //      0        1         2         3        4       5    6       7     8        9    10
        let checkout:string[] = [
              invalid, invalid, 'D1', '1 + D1', 'D2', '1 + D2', 'D3', '3 + D2', 'D4', '1 + D4', 'D5',
                                '3 + D4', 'D6', '5 + D4', 'D7', '7 + D4', 'D8', '1 + D8', 'D9', '3 + D8', 'D10',
                                '5 + D8', 'D11', '7 + D8', 'D12', '1 + D12', 'D13', '3 + D12', 'D14', '5 + D12', 'D15',
                                '7 + D12', 'D16', '1 + D16', 'D17', '3 + D16', 'D18', '5 + D16', 'D19', '7 + D16', 'D20',
                                '9 + D16', '10 + D16', '3 + D20', '12 + D16', '13 + D16', '6 + D20', '15 + D16', '16 + D16', '17 + D16', '10 + D20',

                                '19 + D16', '12 + D20', '13 + D20', '14 + D20', '15 + D20', '16 + D20', '17 + D20', '18 + D20', '19 + D20', '20 + D20',
                                'T15 + D8', 'T10 + D16', 'T13 + D12', 'T16 + D8', 'T19 + D4', 'T10 + D18', 'T17 + D8', 'T20 + D4', '19 + Bull', 'T18 + D8',
                                'T13 + D16', 'T16 + D12', 'T19 + D8', 'T14 + D16', 'T13 + D18', 'T20 + D8', 'T15 + D16', 'T18 + D12', 'T13 + D20', 'T16 + D16',
                                'T15 + D18', 'T14 + D20', 'T17 + D16', 'T16 + D18', 'T15 + D20', 'T18 + D16', 'T17 + D18', 'T16 + D20', 'T19 + D16', 'T18 + D18',
                                'T17 + D20', 'T20 + D16', 'T19 + D18', 'T18 + D20', 'T19 + D19', 'T20 + D18', 'T19 + D20', 'T20 + D19', 'T19 + 10 + D16', 'T20 + D20',

                                'T17 + 10 + D20', 'T20 + 10 + D16', 'T19 + 10 + D18', 'T20 + 12 + D16', 'T20 + 13 + D16', 'T20 + 10 + D18', 'T19 + 10 + D20', 'T20 + 16 + D16', 'T19 + 12 + D20', 'T20 + 10 + D20',
                                'T20 + 19 + D16', 'T20 + 12 + D20', 'T20 + 13 + D20', 'T20 + 14 + D20', 'T20 + 15 + D20', 'T20 + 16 + D20', 'T20 + 17 + D20', 'T20 + 18 + D20', 'T20 + 19 + D20', 'T20 + 20 + D20',
                                'T19 + 14 + Bull', 'T18 + 18 + Bull', 'T20 + T13 + D12', 'T20 + T16 + D8', 'T20 + T19 + D4', 'T19 + 19 + Bull', 'T20 + T17 + D8', 'T20 + T20 + D4', 'T19 + T16 + D12', 'T20 + T18 + D8',
                                'T20 + T13 + D16', 'T20 + T16 + D12', 'T20 + T19 + D8', 'T20 + T14 + D16', 'T20 + T13 + D18', 'T20 + T20 + D8', 'T18 + T17 + D16', 'T20 + T16 + D15', 'T20 + T13 + D20', 'T20 + T16 + D16',
                                'T20 + T15 + D18', 'T20 + T14 + D20', 'T20 + T17 + D16', 'T20 + T20 + D12', 'T20 + T15 + D20', 'T20 + T18 + D16', 'T20 + T17 + D18', 'T20 + T20 + D14', 'T20 + T19 + D16', 'T20 + T18 + D18',

                                'T20 + T17 + D20', 'T20 + T20 + D16', 'T20 + T19 + D18', 'T20 + T18 + D20', 'T20 + T19 + D19', 'T20 + T20 + D18', 'T19 + T20 + D20', 'T20 + T20 + D19', noFinish, 'T20 + T20 + D20',
                                'T20 + T17 + Bull', noFinish, noFinish, 'T19 + T19 + Bull', noFinish, noFinish, 'T20 + T19 + Bull', noFinish, noFinish, 'T20 + T20 + Bull'
                            ]

        return checkout[score];
    }

}
