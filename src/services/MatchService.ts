import { Injectable } from '@angular/core';

@Injectable()
export class MatchService {
    matchEnded:boolean = false;
    setsToWin:number = 5;
    legsToWin:number = 3;
    legPoints:number = 501;

    updateSetsToWin(value:number) {
        this.setsToWin = value;
    }

    updateLegsToWin(value:number) {
        this.legsToWin = value;
    }

    updateLegPoints(value:number) {
        this.legPoints = value;
    }
}
