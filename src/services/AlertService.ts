import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Injectable()
export class AlertService {
    constructor(
        private alertCtrl: AlertController,
    ) {};

    showAlert(title:string, inputs:any[], buttons:any[]):void {
        let alert = this.alertCtrl.create({
            title: title,
            inputs: inputs,
            buttons: buttons
        });

        alert.present();
    }
}
