import { Injectable } from '@angular/core';
import { Player } from '../typings/player';
import { Match } from '../typings/match';
import { MatchService } from '../services/MatchService';

@Injectable()
export class PlayerService {
    initialMatchValues:Match = {sets: 0, legs: 0, score: 0, matchTracker: [[[]]], frequentScores: []};
    players:Player[] = [];
    undoSteps:any[] = [];
    maxUndoSteps:number = 10;
    setStarter:number = 0;
    legStarter:number = 0;
    activePlayer:number = 0; 
    turn:number = 1;
    relativeTurn:number = 1;
    matchEnded:boolean = false;

    constructor(private matchS: MatchService) {};

    setInitialValues() {
        for(let i = 0; i < this.players.length; i++) {
            let match:Match = this.players[i].match;

            match.score = this.matchS.legPoints;
        }
    }

    initializePlayers():void {
        this.players = [];
        this.setStarter = 0;
        this.legStarter = 0;
        this.activePlayer = 0;
        this.turn = 1;
        this.relativeTurn = 1;
    }

    addPlayer(name:string):void {
        let player:Player = {id: this.players.length, name: name, match: JSON.parse(JSON.stringify(this.initialMatchValues)), statistics: {matchAvg: 0}};

        this.players.push(player);
    }

    removePlayer(id:number):void {
        for(let i = 0; i < this.players.length; i++) {
            if(this.players[i].id === id) {
                this.players.splice(i, 1);
                break;
            }
        }
    }

    matchStarted():boolean {
        let playerStarted:boolean[] = [];

        for(let i = 0; i < this.players.length; i++) {
            let match:Match = this.players[i].match;
            if(match.sets === 0 && match.legs === 0 && match.score === this.matchS.legPoints)
                playerStarted.push(false);
            else
                playerStarted.push(true);
        }

        for(let j = 0; j < playerStarted.length; j++) {
            if(playerStarted[j] === true)
                return true;
        }

        return false;
    }

    updateTurn():void {
        this.relativeTurn++;
        if(this.relativeTurn === this.players.length +1) {
            this.turn++;
            this.relativeTurn = 1;
        }
    }

    updateSetStarter() {
        if(this.setStarter === this.players.length -1 ) {
            this.setStarter = 0;
            return;
        }

        this.setStarter++;
    }

    updateLegStarter(setStarter?:number) {
        if(typeof setStarter !== 'undefined') {
            this.legStarter = setStarter;
            return;
        }

        if(this.legStarter === this.players.length -1 ) {
            this.legStarter = 0;
            return;
        }

        this.legStarter++;
    }

    updateActivePlayer(action?:string) {
        if(typeof action !== 'undefined') {
            switch(action) {
                case 'matchWin':
                    this.activePlayer = -1;
                    break;

                case 'setWin':
                    this.legStarter = this.setStarter;
                    this.activePlayer = this.setStarter;
                    break;
                    
                case 'legWin':
                    this.activePlayer = this.legStarter;
                    break;
            }
            return;
        }

        if(this.activePlayer === this.players.length -1) {
            this.activePlayer = 0;
            return;
        }

        this.activePlayer++;
    }

    resetLegPoints() {
        // legs van iedere player op nul zetten
        for(let i = 0; i < this.players.length; i++) {
            this.players[i].match.legs = 0;
        }
    }

    resetScores() {
        // player scores resetten naar legPoints
        for(let i = 0; i < this.players.length; i++) {
            this.players[i].match.score = this.matchS.legPoints;
        }
    }

    saveTurn() {
        let currentStep:any = {}

        currentStep.players = JSON.parse(JSON.stringify(this.players));
        currentStep.turn = this.turn;
        currentStep.relativeTurn = this.relativeTurn;
        currentStep.matchEnded = this.matchEnded;
        currentStep.setStarter = this.setStarter;
        currentStep.legStarter = this.legStarter;
        currentStep.activePlayer = this.activePlayer;

        this.undoSteps.push(currentStep);

        if(this.undoSteps.length > this.maxUndoSteps) {
            this.undoSteps.shift();
        }
    }

    undoTurn() {
        let currentStep:any = this.undoSteps[this.undoSteps.length - 1];

        this.players = JSON.parse(JSON.stringify(currentStep.players));
        this.turn = currentStep.turn;
        this.relativeTurn = currentStep.relativeTurn;
        this.matchEnded = currentStep.matchEnded;
        this.setStarter = currentStep.setStarter;
        this.legStarter = currentStep.legStarter;
        this.activePlayer = currentStep.activePlayer;

        this.undoSteps.pop();
    }
}
