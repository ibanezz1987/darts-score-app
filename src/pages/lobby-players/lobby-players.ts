import { Component, Input } from '@angular/core';
import { ViewController, NavController } from 'ionic-angular';
import { FormGroup, FormBuilder } from "@angular/forms";
import { PlayerService } from '../../services/PlayerService';
import { Main } from '../main/main';

@Component({
    templateUrl: 'lobby-players.html'
})
export class LobbyPlayers {
    mainPage: any = Main;
    playerNameForm: FormGroup;
    @Input() name: string;

    constructor(private formBuilder: FormBuilder, public navCtrl: NavController, private viewCtrl: ViewController, private playerS: PlayerService) {
    };

    ngOnInit() {
        this.playerNameForm = this.formBuilder.group({ name: '' });
    }

    submitPlayerName():void {
        let playerName:string = this.playerNameForm.value.name;

        if(playerName !== '') {
            this.playerS.addPlayer(playerName);
            this.playerNameForm.reset();
        }
    }

    confirmPlayers():void {
        let playerName:string = this.playerNameForm.value.name;

        if(playerName !== undefined && playerName !== null && playerName !== '') {
            playerName = playerName;
            this.playerS.addPlayer(playerName);
            this.playerNameForm.reset();
        }

        // Return to lobby
        this.navCtrl.pop();
    }

    goToMain():void {
        this.navCtrl
          .push(this.mainPage)
          .then(() => {
            // first we find the index of the current view controller:
            const index = this.viewCtrl.index;
            // then we remove it from the navigation stack
            this.navCtrl.remove(index);
          });
    }
}
