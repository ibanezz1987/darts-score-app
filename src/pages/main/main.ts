import { Component, ViewChild } from '@angular/core';
import { NavController, Nav } from 'ionic-angular';
import { MatchService } from '../../services/MatchService';
import { PlayerService } from '../../services/PlayerService';
import { StatisticsService } from '../../services/StatisticsService';
import { CalculationService } from '../../services/CalculationService';
import { AlertService } from '../../services/AlertService';
import { Match } from '../../typings/match';
import { Player } from '../../typings/player';
import { Statistics } from '../statistics/statistics';

@Component({
    templateUrl: 'main.html'
})
export class Main {
    StatisticsPage: any = Statistics;
    @ViewChild(Nav) nav: Nav;
    confirmLeave:boolean;
    throwScore:string = '';
    showFinishInput:boolean = false;
    leavePermission:boolean = false;
    leaveToChild:boolean = false;

    constructor(
      public navCtrl: NavController,
      private matchS: MatchService,
      private playerS: PlayerService,
      private statsS: StatisticsService,
      private calcS: CalculationService,
      private alertS: AlertService,
    ) {}

    ionViewDidEnter() {
        // Reset values when entering
        this.leaveToChild = false;
        this.confirmLeave = true;
        this.leavePermission = false;
        this.playerS.matchEnded = false;
    }

    // Confirm before exiting
    ionViewCanLeave() {
        // Go directly to child
        if(this.leaveToChild) {
            return true; // Leave
        }

        // No confirm if match has ended
        if(this.playerS.matchEnded) {
            this.endGame();
            return true; // Leave
        }

        // Leave after confirmation
        if(this.leavePermission == true) {
            this.endGame();
            return true; // Leave
        }

        // Request confirmation before leave
        if(this.confirmLeave) {
            this.requestLeavePermission();
            return false; // Stay (at least for now)
        }

        // Default (fallback)
        this.endGame();
        return true; // Leave
    }

    requestLeavePermission():void {
        let title:string = 'Are you sure you want to quit this match?';
        let inputs:any[] = [];
        let buttons:any[] = [
          { text: 'Cancel', role: 'cancel', handler: data => {} },
          { text: 'Confirm', handler: data => { 
            this.leavePermission = true;
            this.navCtrl.pop(); // manually call ionViewCanLeave()
           } }
        ];

        this.alertS.showAlert(title, inputs, buttons);
    }

    goToStatistics():void {
        // Don't use confirm before exiting
        this.leaveToChild = true;

        this.navCtrl.push(this.StatisticsPage, {});
    }

    endGame():void {
        this.playerS.initializePlayers();
    }

    updateThrowScore(value:string):void {
        // Wanneer score niet mogelijk is niet weergeven
        if(!this.calcS.validThrowScores[+(+this.throwScore + value)]) {
            return;
        }
        // 0 vervangen door input
        if(+this.throwScore === 0) {
            this.throwScore = value;
            return;
        }
        // Throwscore doorvoeren
        this.throwScore = +this.throwScore + value;
    }

    updateEvaluateThrowScore(value:string):void {
        this.throwScore = value;

        this.evaluateThrowScore();
    }

    clearThrowScore():void {
        this.throwScore = this.throwScore.slice(0, -1);
    }

    evaluateThrowScore():void {
        let match:Match = this.playerS.players[this.playerS.activePlayer].match;
        let remaining:number = match.score - +this.throwScore;
        let validThrowScore:boolean = this.calcS.validThrowScoreCheck(+this.throwScore);
        let validFinishScore:boolean = this.calcS.validFinishScoreCheck(+this.throwScore);

        if(validThrowScore) {
            if(remaining == 0 && validFinishScore) {
                // Finish
                this.showFinishInput = true;
            }
            else if(remaining == 0 && !validFinishScore) {
                // Impossible finish
                let title:string = 'Impossible finish';
                let inputs:any[] = [];
                let buttons:any[] = [ {text: 'Close', role: 'close', handler: data => {}} ];

                this.alertS.showAlert(title, inputs, buttons);
                this.throwScore = '';
            }
            else if(remaining == 1) {
                // Incorrect points remaining
                let title:string = 'Incorrect points remaining';
                let inputs:any[] = [];
                let buttons:any[] = [ {text: 'Close', role: 'close', handler: data => {}} ];

                this.alertS.showAlert(title, inputs, buttons);
                this.throwScore = '';
            }
            else if(remaining > 0) {
                // Valid score
                this.acceptThrowScore();
            }
            else {
                // Invalid score
                let title:string = 'Invalid score';
                let inputs:any[] = [];
                let buttons:any[] = [ {text: 'Close', role: 'close', handler: data => {}} ];
                
                this.alertS.showAlert(title, inputs, buttons);
                this.throwScore = '';
            }
        }
        else {
            let title:string = 'Invalid score';
            let inputs:any[] = [];
            let buttons:any[] = [ {text: 'Close', role: 'close', handler: data => {}} ];

            this.alertS.showAlert(title, inputs, buttons);
            this.throwScore = '';
        }
    }

    updateDartsToFinish(dartsToFinish:number):void {
        this.showFinishInput = false;

        this.acceptThrowScore(dartsToFinish);
    }

    acceptThrowScore(dartsToFinish?:number):void {
        // Save this turn for the purpose of the 'Undo' function
        this.playerS.saveTurn();

        // Set var to acceptThrowScore
        let player:Player = this.playerS.players[this.playerS.activePlayer];
        let match:Match = player.match;
        let hasFinished:boolean = (match.score - +this.throwScore) === 0 ? true : false;
        let arrows:number = typeof(dartsToFinish) !== 'undefined' ? dartsToFinish: 3;
        let isLegStarter:boolean = this.playerS.activePlayer === this.playerS.legStarter;

        // Save scores
        match.matchTracker[match.sets][match.legs].push({score: +this.throwScore, arrows: arrows, hasFinished: hasFinished, isLegStarter: isLegStarter});

        // Update score values
        match.frequentScores = this.statsS.updateFrequentScores(player);
        match.score -= +this.throwScore;
        this.throwScore = '';

        // update turn
        this.playerS.updateTurn();

        // finish score
        if(match.score === 0) {
            match.legs++;
            this.playerS.turn = 1;
            this.playerS.relativeTurn = 1;

            if(this.matchS.legsToWin === match.legs) {
                // Set win
                match.sets++;
                this.playerS.updateSetStarter();
                this.playerS.resetLegPoints();
                this.playerS.resetScores();

                if(this.matchS.setsToWin !== match.sets) {
                    // Set win but not Match win
                    match.matchTracker.push([[]]); // Add new empty set and leg to matchTracker
                    this.playerS.updateActivePlayer('setWin');
                }
                else {
                    // Match win
                    this.pronounceVictory();
                    this.playerS.updateActivePlayer('matchWin');
                    this.playerS.matchEnded = true;
                }
            }
            else {
                // Leg win
                match.matchTracker[match.sets].push([]); // Add new empty leg to matchTracker
                this.playerS.updateLegStarter();
                this.playerS.updateActivePlayer('legWin');
                this.playerS.resetScores();
            }
        }
        else {
            // normal score
            this.playerS.updateActivePlayer();
        }

        // Update statistics
        for(let i=0; i < this.playerS.players.length; i++) {
            let player:Player = this.playerS.players[i];
            this.statsS.calculateStatistics(player);
        }
    }

    pronounceVictory():void {
        let victorer:string = this.playerS.players[this.playerS.activePlayer].name;
        let title:string = 'Congratulations ' + victorer + '! You\'ve won the match!';
        let inputs:any[] = [];
        let buttons:any[] = [ {text: 'Close', role: 'close', handler: data => {}} ];

        this.alertS.showAlert(title, inputs, buttons);
    }

    undo():void {
        let title:string = 'Are you sure you want to undo the last score?';
        let inputs:any[] = [];
        let buttons:any[] = [
          { text: 'Cancel', role: 'cancel', handler: data => {} },
          { text: 'Confirm', handler: data => { 
                // When max undo steps is reached show alert
                this.playerS.undoTurn()
          } }
        ];

        if(this.playerS.undoSteps.length === 0) {
            this.maxUndoMessage();
        } else {
            this.alertS.showAlert(title, inputs, buttons);
        }
    }

    maxUndoMessage():void {
        let title:string = 'Can\'t undo more than ' + this.playerS.maxUndoSteps + ' steps and at the beginning of the match.';
        let inputs:any[] = [];
        let buttons:any[] = [ {text: 'Close', role: 'close', handler: data => {}} ];

        this.alertS.showAlert(title, inputs, buttons);
    }
}
