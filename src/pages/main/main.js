var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { NavController, Nav } from 'ionic-angular';
import { MatchService } from '../../services/MatchService';
import { PlayerService } from '../../services/PlayerService';
import { StatisticsService } from '../../services/StatisticsService';
import { CalculationService } from '../../services/CalculationService';
import { Match } from '../../typings/match';
import { Statistics } from '../statistics/statistics';
var Main = (function () {
    function Main(navCtrl, matchS, playerS, statsS, calcS) {
        this.navCtrl = navCtrl;
        this.matchS = matchS;
        this.playerS = playerS;
        this.statsS = statsS;
        this.calcS = calcS;
        this.StatisticsPage = Statistics;
        this.throwScore = '';
        this.showFinishInput = false;
    }
    ;
    Main.prototype.ionViewDidEnter = function () {
        // Reset confirm before exiting
        this.confirmLeave = true;
    };
    Main.prototype.ionViewCanLeave = function () {
        // Confirm before exiting
        if (this.confirmLeave) {
            if (confirm('Are you sure you want to quit this match?')) {
                this.endGame();
                return true;
            }
            return false;
        }
    };
    Main.prototype.goToStatistics = function () {
        // Don't use confirm before exiting
        this.confirmLeave = false;
        this.navCtrl.push(this.StatisticsPage, {});
    };
    Main.prototype.endGame = function () {
        this.playerS.initializePlayers();
    };
    Main.prototype.updateThrowScore = function (value) {
        // Wanneer score niet mogelijk is niet weergeven
        if (!this.calcS.validThrowScores[+(+this.throwScore + value)]) {
            return;
        }
        // 0 vervangen door input
        if (+this.throwScore === 0) {
            this.throwScore = value;
            return;
        }
        // Throwscore doorvoeren
        this.throwScore = +this.throwScore + value;
    };
    ;
    Main.prototype.updateEvaluateThrowScore = function (value) {
        this.throwScore = value;
        this.evaluateThrowScore();
    };
    ;
    Main.prototype.clearThrowScore = function () {
        this.throwScore = this.throwScore.slice(0, -1);
    };
    ;
    Main.prototype.resetScore = function () {
        if (!confirm('Are you sure you want to reset?')) {
            return;
        }
        var match = new Match;
        if (this.playerS.newLeg) {
            // undo last leg
            var lastPlayer = this.playerS.legs[this.playerS.legs.length - 1];
            this.playerS.activePlayer = lastPlayer;
            match = this.playerS.players[lastPlayer].match;
            // Cancel scores en calculations
            match.legs -= 1;
            for (var i = 0; i < this.playerS.players.length; i++) {
                var match_1 = this.playerS.players[i].match;
                match_1.score = this.playerS.lastLegFinalScores[i];
            }
            this.playerS.newLeg = false;
        }
        else {
            // Undo last throw
            this.playerS.activePlayer--;
            if (this.playerS.activePlayer < 0) {
                this.playerS.activePlayer = this.playerS.players.length - 1;
            }
            match = this.playerS.players[this.playerS.activePlayer].match;
            // Cancel scores en calculations
            match.score += match.matchScores[match.matchScores.length - 1].score;
        }
        // Cancel scores en calculations
        match.matchScores.pop();
        match.frequentScores = this.statsS.updateFrequentScores(match.matchScores);
        match.average = this.calcS.calculateAverage(match.matchScores);
        this.showFinishInput = false;
        this.throwScore = '';
    };
    ;
    Main.prototype.evaluateThrowScore = function () {
        var match = this.playerS.players[this.playerS.activePlayer].match;
        var remaining = match.score - +this.throwScore;
        var validThrowScore = this.calcS.validThrowScoreCheck(+this.throwScore);
        var validFinishScore = this.calcS.validFinishScoreCheck(+this.throwScore);
        if (validThrowScore) {
            if (remaining == 0 && validFinishScore) {
                // finish score
                this.playerS.setLastLegFinalScores();
                this.showFinishInput = true;
            }
            else if (remaining == 0 && !validFinishScore) {
                this.throwScore = '';
                // impossible finish
                alert('impossible finish');
            }
            else if (remaining == 1) {
                this.throwScore = '';
                // incorrect points remaining
                alert('incorrect points remaining');
            }
            else if (remaining > 0) {
                // valid score
                this.acceptThrowScore();
            }
            else {
                this.throwScore = '';
                // negative
                alert('invalid score');
            }
        }
        else {
            this.throwScore = '';
            // impossible throw
            alert('invalid score');
        }
    };
    Main.prototype.updateDartsToFinish = function (dartsToFinish) {
        this.showFinishInput = false;
        this.acceptThrowScore(dartsToFinish);
    };
    Main.prototype.acceptThrowScore = function (dartsToFinish) {
        var match = this.playerS.players[this.playerS.activePlayer].match;
        var hasFinished = match.score === 0 ? true : false;
        var arrows = typeof (dartsToFinish) !== 'undefined' ? dartsToFinish : 3;
        match.matchScores.push({ score: +this.throwScore, arrows: arrows, finish: hasFinished });
        match.frequentScores = this.statsS.updateFrequentScores(match.matchScores);
        match.average = this.calcS.calculateAverage(match.matchScores);
        match.score -= +this.throwScore;
        this.throwScore = '';
        // update turn
        this.playerS.updateTurn();
        if (match.score === 0) {
            // finish score
            match.legs++;
            this.playerS.newLeg = true;
            this.playerS.legs.push(this.playerS.activePlayer);
            this.playerS.turn = 1;
            this.playerS.relativeTurn = 1;
            // if set won then:
            // updateSetStarter()
            this.updateLegStarter();
            this.updateActivePlayer(this.playerS.legStarter);
            this.resetScores();
            console.log('Todo: bijhouden welke speler moet beginnen');
        }
        else {
            // normal score
            this.playerS.newLeg = false;
            this.updateActivePlayer();
        }
    };
    ;
    Main.prototype.resetScores = function () {
        // player scores resetten naar legPoints
        for (var i = 0; i < this.playerS.players.length; i++) {
            this.playerS.players[i].match.score = this.matchS.legPoints;
        }
    };
    Main.prototype.updateSetStarter = function () {
        if (this.playerS.setStarter === this.playerS.players.length - 1) {
            this.playerS.setStarter = 0;
            return;
        }
        this.playerS.setStarter++;
    };
    Main.prototype.updateLegStarter = function (setStarter) {
        if (typeof setStarter !== 'undefined') {
            this.playerS.legStarter = setStarter;
            return;
        }
        if (this.playerS.legStarter === this.playerS.players.length - 1) {
            this.playerS.legStarter = 0;
            return;
        }
        this.playerS.legStarter++;
    };
    Main.prototype.updateActivePlayer = function (legStarter) {
        if (typeof legStarter !== 'undefined') {
            this.playerS.activePlayer = legStarter;
            return;
        }
        if (this.playerS.activePlayer === this.playerS.players.length - 1) {
            this.playerS.activePlayer = 0;
            return;
        }
        this.playerS.activePlayer++;
    };
    return Main;
}());
__decorate([
    ViewChild(Nav),
    __metadata("design:type", Nav)
], Main.prototype, "nav", void 0);
Main = __decorate([
    Component({
        templateUrl: 'main.html'
    }),
    __metadata("design:paramtypes", [NavController,
        MatchService,
        PlayerService,
        StatisticsService,
        CalculationService])
], Main);
export { Main };
//# sourceMappingURL=main.js.map