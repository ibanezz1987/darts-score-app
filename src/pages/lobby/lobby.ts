import { Component, Input } from '@angular/core';
import { ViewController, NavController } from 'ionic-angular';
import { MatchService } from '../../services/MatchService';
import { PlayerService } from '../../services/PlayerService';
import { AlertService } from '../../services/AlertService';
import { Main } from '../main/main';
import { LobbyPlayers } from '../lobby-players/lobby-players';
import { Player } from '../../typings/player';

@Component({
    templateUrl: 'lobby.html'
})
export class Lobby {
    mainPage: any = Main;
    LobbyPlayersPage: any = LobbyPlayers;
    @Input() name: string;

    constructor(
        public navCtrl: NavController,
        private viewCtrl: ViewController,
        private MatchS: MatchService,
        private playerS: PlayerService,
        private alertS: AlertService,
    ) {};

    setPlayers():void {
        this.navCtrl.push(this.LobbyPlayersPage);
    }

    getPlayersString():string {
        let playersString:string = '';
        let players:Player[] = this.playerS.players;
        
        for(let i=0; i < players.length; i++) {
            if(i !== 0) {
                playersString += ' ';
            }

            playersString += players[i].name;

            if(i < players.length - 1) {
                playersString += ',';
            }
        }
        
        return playersString;
    }

    setAmountOfSets():void {
        let title:string = '';
        let inputs:any[] = [{ name: 'noOfSets', placeholder: 'Number of sets', value: this.MatchS.setsToWin }];
        let buttons:any[] = [
          { text: 'Cancel', role: 'cancel', handler: data => {} },
          {
            text: 'Confirm',
            handler: data => {
              let pattern = /^[0-9]*$/;
              if(data.noOfSets === "" || !pattern.test(data.noOfSets) || parseFloat(data.noOfSets) < 1 || parseFloat(data.noOfSets) > 99 ) {
                  return false;
              }
              this.MatchS.updateSetsToWin(+data.noOfSets);
            }
          }
        ];

        this.alertS.showAlert(title, inputs, buttons);
    }

    setAmountOfLegs():void {
        let title:string = '';
        let inputs:any[] = [{ name: 'noOfLegs', placeholder: 'Number of legs', value: this.MatchS.legsToWin }];
        let buttons:any[] = [
          { text: 'Cancel', role: 'cancel', handler: data => {} },
          {
            text: 'Confirm',
            handler: data => {
              let pattern = /^[0-9]*$/;
              if(data.noOfLegs === "" || !pattern.test(data.noOfLegs) || parseFloat(data.noOfLegs) < 1 || parseFloat(data.noOfLegs) > 99 ) {
                  return false;
              }
              this.MatchS.updateLegsToWin(+data.noOfLegs);
            }
          }
        ];

        this.alertS.showAlert(title, inputs, buttons);
    }

    setAmountOfLegPoints():void {
        let title:string = '';
        let inputs:any[] = [{ name: 'noOfLegPoints', placeholder: 'Throw out score', value: this.MatchS.legPoints }];
        let buttons:any[] = [
          { text: 'Cancel', role: 'cancel', handler: data => {} },
          {
            text: 'Confirm',
            handler: data => {
              let pattern = /^[0-9]*$/;
              if(data.noOfLegPoints === "" || !pattern.test(data.noOfLegPoints) || parseFloat(data.noOfLegPoints) > 9999 || parseFloat(data.noOfLegPoints) < 2) {
                  return false;
              }
              this.MatchS.updateLegPoints(+data.noOfLegPoints);
            }
          }
        ];

        this.alertS.showAlert(title, inputs, buttons);
    }

    goToMain():void {
        this.navCtrl
          .push(this.mainPage)
          .then(() => {
            this.playerS.setInitialValues();

            const index = this.viewCtrl.index;
            this.navCtrl.remove(index);
          });
    }

}
