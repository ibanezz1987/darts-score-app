var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { ViewController, NavController } from 'ionic-angular';
import { MatchService } from '../../services/MatchService';
import { PlayerService } from '../../services/PlayerService';
import { AlertService } from '../../services/AlertService';
import { Main } from '../main/main';
import { LobbyPlayers } from '../lobby-players/lobby-players';
var Lobby = (function () {
    function Lobby(navCtrl, viewCtrl, MatchS, playerS, alertS) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.MatchS = MatchS;
        this.playerS = playerS;
        this.alertS = alertS;
        this.mainPage = Main;
        this.LobbyPlayersPage = LobbyPlayers;
    }
    ;
    Lobby.prototype.setPlayers = function () {
        this.navCtrl.push(this.LobbyPlayersPage);
    };
    Lobby.prototype.setAmountOfSets = function () {
        var _this = this;
        var title = '';
        var inputs = [{
                name: 'noOfSets',
                placeholder: 'Number of sets',
                value: this.MatchS.setsToWin
            }];
        var buttons = [
            {
                text: 'Cancel',
                role: 'cancel',
                handler: function (data) { }
            },
            {
                text: 'Confirm',
                handler: function (data) {
                    var pattern = /^[0-9]*$/;
                    if (data.noOfSets === '' || !pattern.test(data.noOfSets)) {
                        return false;
                    }
                    _this.MatchS.updateSetsToWin(data.noOfSets);
                }
            }
        ];
        this.alertS.showAlert(title, inputs, buttons);
    };
    Lobby.prototype.setAmountOfLegs = function () {
        var _this = this;
        var title = '';
        var inputs = [{
                name: 'noOfLegs',
                placeholder: 'Number of legs',
                value: this.MatchS.legsToWin
            }];
        var buttons = [
            {
                text: 'Cancel',
                role: 'cancel',
                handler: function (data) { }
            },
            {
                text: 'Confirm',
                handler: function (data) {
                    var pattern = /^[0-9]*$/;
                    if (data.noOfLegs === '' || !pattern.test(data.noOfLegs)) {
                        return false;
                    }
                    _this.MatchS.updateLegsToWin(data.noOfLegs);
                }
            }
        ];
        this.alertS.showAlert(title, inputs, buttons);
    };
    Lobby.prototype.setAmountOfLegPoints = function () {
        var _this = this;
        var title = '';
        var inputs = [{
                name: 'noOfLegPoints',
                placeholder: 'Throw out score',
                value: this.MatchS.legPoints
            }];
        var buttons = [
            {
                text: 'Cancel',
                role: 'cancel',
                handler: function (data) { }
            },
            {
                text: 'Confirm',
                handler: function (data) {
                    var pattern = /^[0-9]*$/;
                    if (data.noOfLegPoints === '' || !pattern.test(data.noOfLegPoints) || data.noOfLegPoints > 9999 || ) {
                        return false;
                    }
                    _this.MatchS.updateLegPoints(data.noOfLegPoints);
                }
            }
        ];
        this.alertS.showAlert(title, inputs, buttons);
    };
    Lobby.prototype.goToMain = function () {
        var _this = this;
        this.navCtrl
            .push(this.mainPage)
            .then(function () {
            _this.playerS.setInitialValues();
            var index = _this.viewCtrl.index;
            _this.navCtrl.remove(index);
        });
    };
    return Lobby;
}());
__decorate([
    Input(),
    __metadata("design:type", String)
], Lobby.prototype, "name", void 0);
Lobby = __decorate([
    Component({
        templateUrl: 'lobby.html'
    }),
    __metadata("design:paramtypes", [NavController,
        ViewController,
        MatchService,
        PlayerService,
        AlertService])
], Lobby);
export { Lobby };
//# sourceMappingURL=lobby.js.map