import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Lobby } from '../lobby/lobby';

@Component({
    templateUrl: 'home.html'
})
export class Home {
    lobbyPage: any = Lobby;

    constructor(public navCtrl: NavController) {
    };

    goToLobby():void {
        this.navCtrl.push(this.lobbyPage, {});
    }
}
