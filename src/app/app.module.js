var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Home } from '../pages/home/home';
import { Lobby } from '../pages/lobby/lobby';
import { LobbyPlayers } from '../pages/lobby-players/lobby-players';
import { Main } from '../pages/main/main';
import { Records } from '../pages/records/records';
import { Statistics } from '../pages/statistics/statistics';
import { MatchService } from '../services/MatchService';
import { PlayerService } from '../services/PlayerService';
import { StatisticsService } from '../services/StatisticsService';
import { CalculationService } from '../services/CalculationService';
import { AlertService } from '../services/AlertService';
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    NgModule({
        declarations: [
            MyApp,
            Home,
            Lobby,
            LobbyPlayers,
            Main,
            Records,
            Statistics
        ],
        imports: [
            IonicModule.forRoot(MyApp)
        ],
        bootstrap: [IonicApp],
        entryComponents: [
            MyApp,
            Home,
            Lobby,
            LobbyPlayers,
            Main,
            Records,
            Statistics
        ],
        providers: [
            { provide: ErrorHandler, useClass: IonicErrorHandler },
            MatchService, PlayerService, StatisticsService, CalculationService, AlertService
        ]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map