import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Home } from '../pages/home/home';
import { Lobby } from '../pages/lobby/lobby';
import { LobbyPlayers } from '../pages/lobby-players/lobby-players';
import { Main } from '../pages/main/main';
import { Records } from '../pages/records/records';
import { Statistics } from '../pages/statistics/statistics';
import { MatchService } from '../services/MatchService';
import { PlayerService } from '../services/PlayerService';
import { StatisticsService } from '../services/StatisticsService';
import { CalculationService } from '../services/CalculationService';
import { AlertService } from '../services/AlertService';

@NgModule({
  declarations: [
    MyApp,
    Home,
    Lobby,
    LobbyPlayers,
    Main,
    Records,
    Statistics
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Home,
    Lobby,
    LobbyPlayers,
    Main,
    Records,
    Statistics
  ],
  providers: [
      {provide: ErrorHandler, useClass: IonicErrorHandler},
      MatchService, PlayerService, StatisticsService, CalculationService, AlertService
    ]
})
export class AppModule {}
