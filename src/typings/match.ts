export class Match {
    sets: number;
    legs: number;
    score: number;
    matchTracker: {score: number, arrows: number, hasFinished: boolean, isLegStarter: boolean}[][][];
    frequentScores: {value: number, amount: number}[];
}
