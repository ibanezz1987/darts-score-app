import { Match } from '../typings/match';

export class Player {
    id: number;
    name: string;
    match: Match;
    statistics: any;
}
